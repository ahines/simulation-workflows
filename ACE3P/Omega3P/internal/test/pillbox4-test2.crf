<?xml version="1.0"?>
<cmb-resources>
  <attribute id="simbuilder" role="instance">
    <SMTK_AttributeSystem Version="2">
      <!--**********  Category and Analysis Information ***********-->
      <Categories>
        <Cat>Omega3P</Cat>
      </Categories>
      <Analyses>
        <Analysis Type="Omega 3P Analysis">
          <Cat>Omega3P</Cat>
        </Analysis>
      </Analyses>
      <!--**********  Attribute Definitions ***********-->
      <Definitions>
        <AttDef Type="EigenSolver" Label="EigenSolver" BaseType="" Version="0" Unique="true">
          <ItemDefinitions>
            <Int Name="NumEigenvalues" Label="Number of eigenmodes searched" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Omega3P</Cat>
              </Categories>
              <DefaultValue>1</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0</Min>
              </RangeInfo>
            </Int>
            <Double Name="FrequencyShift" Label="Frequency Shift" Version="0" NumberOfRequiredValues="1" Units="Hz">
              <Categories>
                <Cat>Omega3P</Cat>
              </Categories>
              <DefaultValue>1000000000</DefaultValue>
              <RangeInfo>
                <Min Inclusive="true">0</Min>
              </RangeInfo>
            </Double>
          </ItemDefinitions>
        </AttDef>
        <AttDef Type="FEInfo" Label="FEInfo" BaseType="" Version="0" Unique="true">
          <ItemDefinitions>
            <Int Name="Order" Label="Global Order" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Omega3P</Cat>
              </Categories>
              <DefaultValue>2</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0</Min>
              </RangeInfo>
            </Int>
            <Void Name="EnableCurvedSurfaces" Label="Enable Curved Surfaces" Version="0" Optional="true" IsEnabledByDefault="true">
              <Categories>
                <Cat>Omega3P</Cat>
              </Categories>
            </Void>
          </ItemDefinitions>
        </AttDef>
        <AttDef Type="HFormulation" Label="HFormulation" BaseType="" Version="0" Unique="true">
          <ItemDefinitions>
            <Void Name="HFormulation" Label="HFormulation" Version="0" Optional="true" IsEnabledByDefault="false">
              <Categories>
                <Cat>Omega3P</Cat>
              </Categories>
            </Void>
          </ItemDefinitions>
        </AttDef>
        <AttDef Type="Material" Label="Material" BaseType="" Version="0" Unique="true">
          <AssociationsDef Name="MaterialAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
            <Categories>
              <Cat>Omega3P</Cat>
            </Categories>
            <MembershipMask>volume</MembershipMask>
          </AssociationsDef>
          <ItemDefinitions>
            <Double Name="Epsilon" Label="Relative Permittivity" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Omega3P</Cat>
              </Categories>
              <BriefDescription>Real Component of Relative Permittivity</BriefDescription>
              <DefaultValue>1</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0</Min>
              </RangeInfo>
            </Double>
            <Double Name="ImgEpsilon" Label="Imaginary Relative Permittivity" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Omega3P</Cat>
              </Categories>
              <BriefDescription>Imaginary Component of Relative Permittivity associated with material loss</BriefDescription>
              <DefaultValue>0</DefaultValue>
              <RangeInfo>
                <Min Inclusive="true">0</Min>
              </RangeInfo>
            </Double>
            <Double Name="Mu" Label="Relative Permeability" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Omega3P</Cat>
              </Categories>
              <BriefDescription>Real Component of Relative Permeability</BriefDescription>
              <DefaultValue>1</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0</Min>
              </RangeInfo>
            </Double>
            <Double Name="ImgMu" Label="Imaginary Relative Permeability" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Omega3P</Cat>
              </Categories>
              <BriefDescription>Imaginary Component of Relative Permeability associated with material loss</BriefDescription>
              <DefaultValue>0</DefaultValue>
              <RangeInfo>
                <Min Inclusive="true">0</Min>
              </RangeInfo>
            </Double>
          </ItemDefinitions>
        </AttDef>
        <AttDef Type="PostProcess" Label="Post Process" BaseType="" Version="0" Unique="true">
          <ItemDefinitions>
            <Group Name="Toggle" Label="Write Post Process Files" Version="1" Optional="true" IsEnabledByDefault="true" NumberOfRequiredGroups="1">
              <ItemDefinitions>
                <String Name="ModeFilePrefix" Label="Mode Files Prefix" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredValues="1">
                  <Categories>
                    <Cat>Omega3P</Cat>
                  </Categories>
                  <BriefDescription>Prefix to use in labeling output *.mod files</BriefDescription>
                  <DefaultValue>mode</DefaultValue>
                </String>
              </ItemDefinitions>
            </Group>
          </ItemDefinitions>
        </AttDef>
        <AttDef Type="RegionHighOrder" Label="RegionHighOrder" BaseType="" Version="0" Unique="true">
          <AssociationsDef Name="MaterialAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
            <Categories>
              <Cat>Omega3P</Cat>
            </Categories>
            <MembershipMask>volume</MembershipMask>
          </AssociationsDef>
          <ItemDefinitions>
            <Int Name="RegionHighOrder" Label="Region High Order" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Omega3P</Cat>
              </Categories>
              <DefaultValue>2</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0</Min>
              </RangeInfo>
            </Int>
          </ItemDefinitions>
        </AttDef>
        <AttDef Type="SurfaceProperty" Label="Surface Boundary Condition" BaseType="" Version="0" Unique="true">
          <AssociationsDef Name="SurfacePropertyAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
            <Categories>
              <Cat>Omega3P</Cat>
            </Categories>
            <MembershipMask>face</MembershipMask>
          </AssociationsDef>
          <ItemDefinitions>
            <String Name="Type" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Omega3P</Cat>
              </Categories>
              <BriefDescription>Indicates the type of surface boundary condition</BriefDescription>
              <ChildrenDefinitions>
                <ModelEntity Name="MasterSurface" Label="Master Surface" Version="0" NumberOfRequiredValues="1">
                  <Categories>
                    <Cat>Omega3P</Cat>
                  </Categories>
                  <MembershipMask>face</MembershipMask>
                  <ComponentLabels>
                    <Label />
                  </ComponentLabels>
                </ModelEntity>
                <Int Name="NumModes" Label="Number of Modes" Version="0" NumberOfRequiredValues="1">
                  <Categories>
                    <Cat>Omega3P</Cat>
                  </Categories>
                  <BriefDescription>Number of Modes Loaded on Port</BriefDescription>
                  <DefaultValue>1</DefaultValue>
                  <RangeInfo>
                    <Min Inclusive="true">0</Min>
                  </RangeInfo>
                </Int>
                <Double Name="Sigma" Label="Conductivity" Version="0" NumberOfRequiredValues="1" Units="s/m">
                  <Categories>
                    <Cat>Omega3P</Cat>
                  </Categories>
                  <BriefDescription>Impedance Surface Conductivity.</BriefDescription>
                  <DefaultValue>58000000</DefaultValue>
                </Double>
                <Double Name="Theta" Label="Theta (Relative Phase Angle)" Version="0" NumberOfRequiredValues="1">
                  <Categories>
                    <Cat>Omega3P</Cat>
                  </Categories>
                  <BriefDescription>Relative Phase Between Master and Slave Surfaces</BriefDescription>
                  <DefaultValue>0</DefaultValue>
                </Double>
              </ChildrenDefinitions>
              <DiscreteInfo>
                <Value Enum="Electric">Electric</Value>
                <Value Enum="Magnetic">Magnetic</Value>
                <Structure>
                  <Value Enum="Exterior">Exterior</Value>
                  <Items>
                    <Item>Sigma</Item>
                  </Items>
                </Structure>
                <Structure>
                  <Value Enum="Impedance">Impedance</Value>
                  <Items>
                    <Item>Sigma</Item>
                  </Items>
                </Structure>
                <Value Enum="Absorbing">Absorbing</Value>
                <Structure>
                  <Value Enum="Port">Waveguide</Value>
                  <Items>
                    <Item>NumModes</Item>
                  </Items>
                </Structure>
                <Structure>
                  <Value Enum="Periodic">Periodic</Value>
                  <Items>
                    <Item>MasterSurface</Item>
                    <Item>Theta</Item>
                  </Items>
                </Structure>
              </DiscreteInfo>
            </String>
          </ItemDefinitions>
        </AttDef>
        <AttDef Type="Tolerant" Label="Tolerant" BaseType="" Version="0" Unique="true">
          <ItemDefinitions>
            <Void Name="Tolerant" Label="Tolerant" Version="0" Optional="true" IsEnabledByDefault="false">
              <Categories>
                <Cat>Omega3P</Cat>
              </Categories>
            </Void>
          </ItemDefinitions>
        </AttDef>
      </Definitions>
      <!--**********  Attribute Instances ***********-->
      <Attributes>
        <Att Name="EigenSolver" Type="EigenSolver" ID="081c8897-ac97-43e2-b907-f0ac9b20dcd8">
          <Items>
            <Int Name="NumEigenvalues">1</Int>
            <Double Name="FrequencyShift">1000000000</Double>
          </Items>
        </Att>
        <Att Name="Finite Element Info" Type="FEInfo" ID="e65a47fd-ff58-403f-8521-ad18af83901b">
          <Items>
            <Int Name="Order">2</Int>
            <Void Name="EnableCurvedSurfaces" Enabled="true" />
          </Items>
        </Att>
        <Att Name="HForumulation" Type="HFormulation" ID="7a3047af-926d-4461-ae8e-0e8063c5ca14">
          <Items>
            <Void Name="HFormulation" Enabled="false" />
          </Items>
        </Att>
        <Att Name="Material-0" Type="Material" ID="7fa74d90-8130-4658-a71d-4a6cb7ad3103">
          <Associations Name="MaterialAssociations" NumberOfValues="1">
            <Values>
              <Val Ith="0">2e5d6bbe-b8ef-4674-a8b1-722712202f63</Val>
            </Values>
          </Associations>
          <Items>
            <Double Name="Epsilon">0.97499999999999998</Double>
            <Double Name="ImgEpsilon" Enabled="false">0</Double>
            <Double Name="Mu">0.59999999999999998</Double>
            <Double Name="ImgMu" Enabled="false">0</Double>
          </Items>
        </Att>
        <Att Name="PostProcess" Type="PostProcess" ID="db87ac49-28b4-4557-950f-53ced19bd034">
          <Items>
            <Group Name="Toggle" Enabled="true">
              <String Name="ModeFilePrefix" Enabled="true">mfile</String>
            </Group>
          </Items>
        </Att>
        <Att Name="SurfaceProperty-0" Type="SurfaceProperty" ID="4f1b20c1-a6cb-47db-85c8-259d6aa49609">
          <Associations Name="SurfacePropertyAssociations" NumberOfValues="1">
            <Values>
              <Val Ith="0">db002241-9eee-49ba-a2b7-d50e3a44264c</Val>
            </Values>
          </Associations>
          <Items>
            <String Name="Type" Discrete="true">
              <ChildrenItems>
                <ModelEntity Name="MasterSurface" NumberOfValues="1" />
                <Int Name="NumModes">1</Int>
                <Double Name="Sigma">58000000</Double>
                <Double Name="Theta">0</Double>
              </ChildrenItems>3</String>
          </Items>
        </Att>
        <Att Name="SurfaceProperty-1" Type="SurfaceProperty" ID="90671ae9-9890-45f0-be29-e52da1904e31">
          <Associations Name="SurfacePropertyAssociations" NumberOfValues="1">
            <Values>
              <Val Ith="0">ddd6f368-145f-4788-a83d-c48ab8f6e002</Val>
            </Values>
          </Associations>
          <Items>
            <String Name="Type" Discrete="true">
              <ChildrenItems>
                <ModelEntity Name="MasterSurface" NumberOfValues="1" />
                <Int Name="NumModes">2</Int>
                <Double Name="Sigma">58000000</Double>
                <Double Name="Theta">0</Double>
              </ChildrenItems>5</String>
          </Items>
        </Att>
        <Att Name="Tolerant" Type="Tolerant" ID="f3ab74d1-846a-4041-8ff1-8fdca357a38b">
          <Items>
            <Void Name="Tolerant" Enabled="true" />
          </Items>
        </Att>
      </Attributes>
      <!--********** Workflow Views ***********-->
      <Views>
        <View Type="Group" Title="SimBuilder" FilterByAdvanceLevel="false" FilterByCategory="false" TabPosition="North" TopLevel="true">
          <DefaultColor>1., 1., 0.5, 1.</DefaultColor>
          <InvalidColor>1, 0.5, 0.5, 1</InvalidColor>
          <AdvancedFontEffects />
          <Views>
            <View Title="Boundary Conditions" />
            <View Title="Materials" />
            <View Title="Analysis" />
          </Views>
        </View>
        <View Type="Group" Title="Analysis" Style="Tiled">
          <Views>
            <View Title="Tolerant" />
            <View Title="FiniteElement" />
            <View Title="EigenSolver" />
            <View Title="Post Process" />
            <View Title="High Order Regions" />
          </Views>
        </View>
        <View Type="Group" Title="Boundary Conditions" Style="Tiled">
          <Views>
            <View Title="HFormulation" />
            <View Title="Surface Properties" />
          </Views>
        </View>
        <View Type="Instanced" Title="EigenSolver">
          <InstancedAttributes>
            <Att Name="EigenSolver" Type="EigenSolver" />
          </InstancedAttributes>
        </View>
        <View Type="Instanced" Title="FiniteElement">
          <InstancedAttributes>
            <Att Name="Finite Element Info" Type="FEInfo" />
          </InstancedAttributes>
        </View>
        <View Type="Instanced" Title="HFormulation">
          <InstancedAttributes>
            <Att Name="HForumulation" Type="HFormulation" />
          </InstancedAttributes>
        </View>
        <View Type="Attribute" Title="High Order Regions" ModelEntityFilter="r">
          <AttributeTypes>
            <Att Type="RegionHighOrder" />
          </AttributeTypes>
        </View>
        <View Type="Attribute" Title="Materials" ModelEntityFilter="r">
          <AttributeTypes>
            <Att Type="Material" />
          </AttributeTypes>
        </View>
        <View Type="Instanced" Title="Post Process">
          <InstancedAttributes>
            <Att Name="PostProcess" Type="PostProcess" />
          </InstancedAttributes>
        </View>
        <View Type="Attribute" Title="Surface Properties" ModelEntityFilter="f">
          <AttributeTypes>
            <Att Type="SurfaceProperty" />
          </AttributeTypes>
        </View>
        <View Type="Instanced" Title="Tolerant">
          <InstancedAttributes>
            <Att Name="Tolerant" Type="Tolerant" />
          </InstancedAttributes>
        </View>
      </Views>
    </SMTK_AttributeSystem>
  </attribute>
  <attribute id="export" role="template">
    <SMTK_AttributeSystem Version="2">
      <!--**********  Category and Analysis Information ***********-->
      <!--**********  Attribute Definitions ***********-->
      <Definitions>
        <AttDef Type="ExportSpec" Label="Settings" BaseType="" Version="0" Unique="true">
          <ItemDefinitions>
            <String Name="AnalysisTypes" Label="Analysis Types" Version="0" AdvanceLevel="99" NumberOfRequiredValues="1" Extensible="true" />
            <File Name="OutputFile" Label="Output File (*.o3p, *.omega3p)" Version="0" NumberOfRequiredValues="1" FileFilters="Omega3P files (*.o3p *.omega3p);;All files (*.*)" />
            <File Name="PythonScript" Label="Python script" Version="0" NumberOfRequiredValues="1" ShouldExist="true" FileFilters="Python files (*.py);;All files (*.*)" />
            <Group Name="NERSCSimulation" Label="Submit job to NERSC" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredGroups="1">
              <ItemDefinitions>
                <String Name="JobName" Label="Job name" Version="0" NumberOfRequiredValues="1">
                  <BriefDescription>Label you can use to track your job</BriefDescription>
                  <DefaultValue>Omega3P</DefaultValue>
                </String>
                <String Name="CumulusHost" Label="Cumulus host" Version="0" NumberOfRequiredValues="1">
                  <DefaultValue>http://localhost:8080</DefaultValue>
                </String>
                <String Name="NERSCRepository" Label="Project repository" Version="0" NumberOfRequiredValues="1" />
                <String Name="NERSCAccountName" Label="NERSC account name" Version="0" NumberOfRequiredValues="1" />
                <String Name="NERSCAccountPassword" Label="NERSC account password" Version="0" NumberOfRequiredValues="1" Secure="true" />
                <String Name="Machine" Label="NERSC Machine" Version="0" NumberOfRequiredValues="1">
                  <DiscreteInfo DefaultIndex="0">
                    <Value Enum="Cori (Cray XC40)">cori</Value>
                    <Value Enum="Edison (Cray XC30)">edison</Value>
                  </DiscreteInfo>
                </String>
                <String Name="JobDirectory" Label="Job directory" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredValues="1">
                  <BriefDescription>Typically this should be set to the absolute path
to the Scratch directory. ModelBuilder will create a separate
subdirectory for each job.
              </BriefDescription>
                </String>
                <String Name="Queue" Label="Queue" Version="0" NumberOfRequiredValues="1">
                  <DiscreteInfo DefaultIndex="0">
                    <Value Enum="debug">debug</Value>
                    <Value Enum="regular">normal</Value>
                    <Value Enum="premium">premium</Value>
                    <Value Enum="low priority">low</Value>
                    <Value Enum="scavenger">scavenger</Value>
                  </DiscreteInfo>
                </String>
                <Int Name="NumberOfNodes" Label="Number of nodes" Version="0" NumberOfRequiredValues="1">
                  <DefaultValue>1</DefaultValue>
                  <RangeInfo>
                    <Min Inclusive="true">1</Min>
                  </RangeInfo>
                </Int>
                <Int Name="NumberOfTasks" Label="Number of cores" Version="0" NumberOfRequiredValues="1">
                  <DefaultValue>1</DefaultValue>
                  <RangeInfo>
                    <Min Inclusive="true">1</Min>
                  </RangeInfo>
                </Int>
                <Int Name="Timeout" Label="Time limit" Version="0" NumberOfRequiredValues="1" Units="min">
                  <DefaultValue>5</DefaultValue>
                  <RangeInfo>
                    <Min Inclusive="true">1</Min>
                  </RangeInfo>
                </Int>
                <String Name="TailFile" Label="Tail Filename" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
                  <DefaultValue>omega3p_results/omega3p.log</DefaultValue>
                </String>
              </ItemDefinitions>
            </Group>
          </ItemDefinitions>
        </AttDef>
      </Definitions>
      <!--**********  Attribute Instances ***********-->
      <Attributes />
      <!--********** Workflow Views ***********-->
      <Views>
        <View Type="Group" Title="Export" FilterByCategory="false" Style="Tiled" TopLevel="true">
          <Views>
            <View Title="Export Settings" />
          </Views>
        </View>
        <View Type="Instanced" Title="Export Settings">
          <InstancedAttributes>
            <Att Name="Options" Type="ExportSpec" />
          </InstancedAttributes>
        </View>
      </Views>
    </SMTK_AttributeSystem>
  </attribute>
</cmb-resources>

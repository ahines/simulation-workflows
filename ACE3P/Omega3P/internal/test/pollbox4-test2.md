# Test Conditions in pillbox4-test1.crf

## Boundary conditions
SurfaceProperty - Type Exterior
* side set 6
* Conductivity 5.811e+070

Surface Property - Type Magnetic
* side set 1, side set 2

## Materials
Material
* element block 1
* Relative Permittivity 0.99
* Imaginary Relative Permittivity 0.98

## Analysis
EigenSolver
* Number of eigenmodes searched: 4

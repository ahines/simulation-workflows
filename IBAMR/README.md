## IBAMR
This directory is for CMB templates and python scripts for IBAMR,
a distributed-memory parallel implementation of the immersed boundary (IB)
method, with support for Cartesian grid adaptive mesh refinement (AMR).
See https://github.com/IBAMR/IBAMR


**THESE CONTENTS ARE NOT COMPLETE AND SUBJECT TO CHANGE.**

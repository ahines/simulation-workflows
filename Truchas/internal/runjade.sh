#!/usr/bin/env bash

# List of templates in subfolder
declare -a names=( \
  "body-source-probe" \
  "boundary-condition" \
  "thermal-surface-condition" \
  "material" \
  "numerics-solver" \
  "enclosure" \
  "other" \
  )
subfolder="templates"

for name in ${names[@]}; do
  #echo ${name}
  rm -f ${subfolder}/${name}.sbt
  jade ${subfolder}/${name}.jade --pretty --extension sbt
  python fixmultilinetext.py ${subfolder}/${name}.sbt
done

<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeSystem Version="2">
  <Categories>
    <Cat>Heat Transfer</Cat>
    <Cat>Fluid Flow</Cat>
  </Categories>
  <!-- Attribute Definitions-->
  <Definitions>
    <!-- BODY-->
    <AttDef Type="body" Label="Body (Initial Conditions)" BaseType="" Version="0">
      <AssociationsDef Name="DSSourceAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>volume</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Double Name="temperature" Label="Temperature">
          <BriefDescription>Initial temperature of the material body</BriefDescription>
          <Categories>
            <Cat>Fluid Flow</Cat>
            <Cat>Heat Transfer</Cat>
          </Categories>
          <ExpressionType>tabular-function</ExpressionType>
        </Double>
        <Double Name="velocity" Label="Velocity" NumberOfRequiredValues="3">
          <BriefDescription>Initial velocity of the material body</BriefDescription>
          <Categories>
            <Cat>Fluid Flow</Cat>
            <Cat>Heat Transfer</Cat>
          </Categories>
          <DefaultValue>0.0</DefaultValue>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <!-- DS_SOURCE-->
    <AttDef Type="ds-source" Label="Volumetric Heat Source" BaseType="" Version="0">
      <AssociationsDef Name="DSSourceAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>volume</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Double Name="source" Label="Temperature">
          <Categories>
            <Cat>Heat Transfer</Cat>
          </Categories>
          <ExpressionType>tabular-function</ExpressionType>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <!-- PROBE-->
    <AttDef Type="probe" Label="Probe" BaseType="" Version="0">
      <ItemDefinitions>
        <String Name="description" Label="Description" MultipleLInes="true" Optional="true" IsEnabledByDefault="false"></String>
        <Double Name="coords" Label="Coordinates" NumberOfRequiredValues="3">
          <BriefDescription>Used to define the location of the probe</BriefDescription>
          <ComponentLabels>
            <Label>x:</Label>
            <Label>y:</Label>
            <Label>z:</Label>
          </ComponentLabels>
        </Double>
        <Double Name="scale" Label="Scale">
          <DefaultValue>1.0</DefaultValue>
        </Double>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeSystem>

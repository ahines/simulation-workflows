<SMTK_AttributeSystem Version="2">
  <Categories>
    <Cat>Heat Transfer</Cat>
    <Cat>Fluid Flow</Cat>
  </Categories>
  <Definitions>
    <!-- Numerics-->
    <AttDef Type="numerics" Label="Numerics" BaseType="" Version="0" Unique="true">
      <ItemDefinitions>
        <Double Name="dt_init" Label="Dt_Init">
          <BriefDescription>Integration time step value used for
the first computation cycle</BriefDescription>
          <DefaultValue>1.0e-6</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0.0</Min>
          </RangeInfo>
        </Double>
        <Double Name="dt_grow" Label="Dt_Grow">
          <BriefDescription>A factor to multiply the current integration time step
when estimating the next cycle time step.</BriefDescription>
          <DefaultValue>1.05</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">1.0</Min>
          </RangeInfo>
        </Double>
        <Double Name="dt_max" Label="Dt_Max">
          <BriefDescription>Maximum allowable value for the time step.</BriefDescription>
          <DefaultValue>10.0</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0.0</Min>
          </RangeInfo>
        </Double>
        <Double Name="dt_min" Label="Dt_Min">
          <BriefDescription>Minimum allowable value for the time step.</BriefDescription>
          <DefaultValue>1.0e-6</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0.0</Min>
          </RangeInfo>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <!-- Simulation Control-->
    <AttDef Type="simulation-control" Label="Simulation Control" BaseType="" Version="0" Unique="true">
      <ItemDefinitions>
        <Group Name="simulation-control" Label="Simulation Control (for non-radiation only)" Optional="true" IsEnabledByDefault="false">
          <BriefDescription>This item is superseded by the displacement sequence when
moving-enclosure radiation is enabled</BriefDescription>
          <ItemDefinitions>
            <Double Name="phase-start-times" Label="Phase Start Times" Extensible="true">
              <BriefDescription>The list of starting times of each of the phases</BriefDescription>
            </Double>
            <Double Name="phase-init-dt-factor" Label="Phase Init Dt Factor"/>
          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>
    <!-- Solver Settings-->
    <AttDef Type="solver" Label="Solver" BaseType="" Version="0" Unique="true">
      <ItemDefinitions>
        <String Name="analysis" Label="Analysis Type">
          <ChildrenDefinitions>
            <!-- Thermal-only case-->
            <Group Name="thermal" Label="Thermal Only">
              <ItemDefinitions>
                <Double Name="abs-enthalpy-tol" Label="Absolute enthalpy tolerance">
                  <BriefDescription>The tolerance for the absolute error component of the
enthalpy error norm used by the BDF2 integrator</BriefDescription>
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                  <RangeInfo>
                    <Main Inclusive="true">0.0</Main>
                  </RangeInfo>
                </Double>
                <Double Name="rel-enthalpy-tol" Label="Relative enthalpy tolerance">
                  <BriefDescription>The tolerance for the relative error component of the
enthalpy error norm used by the BDF2 integrator</BriefDescription>
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                  <DefaultValue>0.0</DefaultValue>
                  <RangeInfo>
                    <Min Inclusive="true">0.0</Min>
                    <Max Inclusive="true">1.0</Max>
                  </RangeInfo>
                </Double>
                <Double Name="abs-temperature-tol" Label="Absolute temperature tolerance">
                  <BriefDescription>The tolerance for the absolute error component of the
temperature error norm used by the BDF2 integrator</BriefDescription>
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                  <RangeInfo>
                    <Main Inclusive="true">0.0</Main>
                  </RangeInfo>
                </Double>
                <Double Name="rel-temperature-tol" Label="Relative temperature tolerance">
                  <BriefDescription>The tolerance for the relative error component of the
temperature error norm used by the BDF2 integrator</BriefDescription>
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                  <DefaultValue>0.0</DefaultValue>
                  <RangeInfo>
                    <Min Inclusive="true">0.0</Min>
                    <Max Inclusive="true">1.0</Max>
                  </RangeInfo>
                </Double>
                <Void Name="verbose-stepping" Label="Verbose stepping" Optional="true" IsEnabledByDefault="false">
                  <BriefDescription>Enables the output of detailed BDF2 time stepping information</BriefDescription>
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                    <Cat>Fluid Flow</Cat>
                  </Categories>
                </Void>
                <Int Name="max-nlk-itr" Label="Max NLK iterations" AdvanceLevel="1">
                  <BriefDescription>The maximum number of NLK nonlinear solver iterations allowed</BriefDescription>
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                  <DefaultValue>5</DefaultValue>
                  <RangeInfo>
                    <Min Inclusive="true">2</Min>
                  </RangeInfo>
                </Int>
                <Int Name="max-nlk-vec" Label="Max NLK vectors" AdvanceLevel="1">
                  <BriefDescription>The maximum number of acceleration vectors to use
in the NLK nonlinear solver</BriefDescription>
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                  <DefaultValue>4</DefaultValue>
                  <RangeInfo>
                    <Min Inclusive="true">1</Min>
                  </RangeInfo>
                </Int>
                <Double Name="nlk-tol" Label="NLK convergence tolerance" AdvanceLevel="1">
                  <BriefDescription>The convergence tolerance for the NLK nonlinear solver</BriefDescription>
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                  <DefaultValue>0.1</DefaultValue>
                  <RangeInfo>
                    <Min Inclusive="true">0.0</Min>
                    <Max Inclusive="true">1.0</Max>
                  </RangeInfo>
                </Double>
                <Double Name="nlk-vec-tol" Label="NLK vector drop tolerance" AdvanceLevel="1">
                  <BriefDescription>The NLK vector drop tolerance</BriefDescription>
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                  <DefaultValue>0.001</DefaultValue>
                  <RangeInfo>
                    <Min Inclusive="false">0.0</Min>
                  </RangeInfo>
                </Double>
                <Int Name="pc-amg-cycles" Label="Preconditioning V-cycles" AdvanceLevel="1">
                  <BriefDescription>The number of V-cycles to take per preconditioning step
of the nonlinear iteration</BriefDescription>
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                  <DefaultValue>1</DefaultValue>
                  <RangeInfo>
                    <Min Inclusive="true">1</Min>
                  </RangeInfo>
                </Int>
              </ItemDefinitions>
            </Group>
            <!-- Fluid-only case-->
            <Group Name="fluid" Label="Fluid Only">
              <ItemDefinitions>
                <Group Name="flow-numerics" Label="Flow Numerics">
                  <ItemDefinitions>
                    <String Name="discrete-ops-type" Label="Discrete_Ops_type">
                      <BriefDescription>Numerical reconstruction method for estimating face-centered
spatial graients and values of discrete cell-centered data.</BriefDescription>
                      <DiscreteInfo DefaultIndex="0">
                        <Value Enum="Default">default</Value>
                        <Value Enum="Ortho">ortho</Value>
                        <Value Enum="Non-ortho">nonortho</Value>
                      </DiscreteInfo>
                      <Categories>
                        <Cat>Fluid Flow</Cat>
                      </Categories>
                    </String>
                    <Double Name="viscous-number" Label="Viscous Number">
                      <RangeInfo>
                        <Min Inclusive="true">0.0</Min>
                      </RangeInfo>
                    </Double>
                    <Int Name="volume-track-subcycles" Label="Volume Track Subcycles">
                      <BriefDescription>Volume tracking time step specification, expressed as a
multiplicative factor relative to the time step</BriefDescription>
                      <DefaultValue>2</DefaultValue>
                      <RangeInfo>
                        <Min Inclusive="false">0</Min>
                        <Max Inclusive="true">20</Max>
                      </RangeInfo>
                    </Int>
                    <Double Name="body-force-implicitness" Label="Body Force Implicitness" AdvanceLevel="1">
                      <BriefDescription>Degree of time implicitness used for the body-forces in the
discrete form of the momentum equations</BriefDescription>
                      <DefaultValue>0.5</DefaultValue>
                      <RangeInfo>
                        <Min Inclusive="true">0.0</Min>
                        <Max Inclusive="true">1.0</Max>
                      </RangeInfo>
                    </Double>
                    <Group Name="mass-limiter" Label="Mass Limiter" AdvanceLevel="1" Optional="true" IsEnabledByDefault="false">
                      <BriefDescription>Used to avoid the generation of excessively large velocities
in partially-filled fluid-void cells where the fluid mass
approaches the cutoff volume-fraction</BriefDescription>
                      <ItemDefinitions>
                        <Double Name="mass-limiter-cutoff" Label="Mass Limiter Cutoff">
                          <BriefDescription>Used to specify the the value of a material cell volume
fraction below which mass is ignored</BriefDescription>
                          <DefaultValue>1.0e-5</DefaultValue>
                          <RangeInfo>
                            <Max Inclusive="false">1.0</Max>
                          </RangeInfo>
                        </Double>
                      </ItemDefinitions>
                    </Group>
                    <Double Name="mechanical-energy-bound" Label="Mechanical Energy Bound" AdvanceLevel="1">
                      <BriefDescription>Control option for applying the body force at cell faces
during the velocity correction step</BriefDescription>
                      <ChildrenDefinitions>
                        <Double Name="value" Label="Value"/>
                      </ChildrenDefinitions>
                      <DiscreteInfo DefaultIndex="0">
                        <Value Enum="infinity"/>
                        <Structure>
                          <Value Enum="value"/>
                          <Items>
                            <Item>value</Item>
                          </Items>
                        </Structure>
                      </DiscreteInfo>
                    </Double>
                    <Double Name="momentum-solidify-implicitness" Label="Momentum Solidify Implicitness" AdvanceLevel="1">
                      <BriefDescription>Degree of time implicitness used for the velocity field in the
treatment of the momentum deposition associated with solidification</BriefDescription>
                      <DefaultValue>0.0</DefaultValue>
                      <RangeInfo>
                        <Min Inclusive="true">0.0</Min>
                        <Max Inclusive="true">1.0</Max>
                      </RangeInfo>
                    </Double>
                  </ItemDefinitions>
                </Group>
                <Group Name="projection-linear-solver" Label="Projection Linear Solver">
                  <ItemDefinitions>
                    <String Name="method" Label="Method">
                      <BriefDescription>Algorithm used for solution of linear systems</BriefDescription>
                      <DiscreteInfo DefaultIndex="0">
                        <Value>fgmres</Value>
                      </DiscreteInfo>
                    </String>
                    <String Name="preconditioning-method" Label="Preconditioning Method">
                      <BriefDescription>Algorithm used to precondition the Krylov iteration</BriefDescription>
                      <DiscreteInfo DefaultIndex="0">
                        <Value>ssor</Value>
                      </DiscreteInfo>
                    </String>
                    <Int Name="preconditioning-steps" Label="Preconditioning Steps">
                      <BriefDescription>Number of passes to be performed in Jacobi or SSOR preconditioning</BriefDescription>
                      <DefaultValue>2</DefaultValue>
                      <RangeInfo>
                        <Min Inclusive="false">0</Min>
                      </RangeInfo>
                    </Int>
                    <Double Name="relaxation-parameter" Label="Relaxation Parameter">
                      <BriefDescription>Relaxation parameter used in Jacobi and SSOR preconditioning</BriefDescription>
                      <DefaultValue>1.4</DefaultValue>
                      <RangeInfo>
                        <Min Inclusive="false">0.0</Min>
                        <Max Inclusive="false">2.0</Max>
                      </RangeInfo>
                    </Double>
                    <String Name="stopping-criterion" Label="Stopping Criterion">
                      <BriefDescription>Test used to estimate error and determine when convergence
has been reached</BriefDescription>
                      <DiscreteInfo DefaultIndex="0">
                        <Value>||r||</Value>
                        <Value>||r||/||b||</Value>
                      </DiscreteInfo>
                    </String>
                    <Double Name="convergence-criterion" Label="Convergence Criterion">
                      <BriefDescription>Value of error estimate used to determine when convergence
has been reached</BriefDescription>
                      <DefaultValue>1.0e-8</DefaultValue>
                      <RangeInfo>
                        <Min Inclusive="false">0.0</Min>
                        <Max Inclusive="false">0.1</Max>
                      </RangeInfo>
                    </Double>
                    <Int Name="krylov-vectors" Label="Krylov Vectors" AdvanceLevel="1">
                      <BriefDescription>Number of vectors used by GMRES and FGMRES in orthogonalization</BriefDescription>
                      <DefaultValue>50</DefaultValue>
                      <RangeInfo>
                        <Main Inclusive="false">0</Main>
                      </RangeInfo>
                    </Int>
                    <String Name="output-mode" Label="Output Mode" AdvanceLevel="1">
                      <BriefDescription>Controls verbosity of linear solver</BriefDescription>
                      <DiscreteInfo DefaultIndex="0">
                        <Value>none</Value>
                        <Value>errors</Value>
                        <Value>errors+warnings</Value>
                        <Value>summary</Value>
                        <Value>iterates</Value>
                        <Value>full</Value>
                      </DiscreteInfo>
                    </String>
                  </ItemDefinitions>
                </Group>
                <Group Name="viscous-flow-model" Label="Viscous Flow Model" Optional="true" IsEnabledByDefault="false">
                  <ItemDefinitions>
                    <Double Name="viscous-implicitness" Label="Viscous Implicitness">
                      <BriefDescription>Degree of time implicitness used in velocity field calculations</BriefDescription>
                      <DefaultValue>0.0</DefaultValue>
                      <RangeInfo>
                        <Min Inclusive="true">0.0</Min>
                        <Max Inclusive="true">1.0</Max>
                      </RangeInfo>
                    </Double>
                    <Group Name="viscous-linear-solver" Label="Viscous Linear Solver">
                      <ItemDefinitions>
                        <String Name="method" Label="Method">
                          <BriefDescription>Algorithm used for solution of linear systems</BriefDescription>
                          <DiscreteInfo DefaultIndex="0">
                            <Value>fgmres</Value>
                          </DiscreteInfo>
                        </String>
                        <String Name="preconditioning-method" Label="Preconditioning Method">
                          <BriefDescription>Algorithm used to precondition the Krylov iteration</BriefDescription>
                          <DiscreteInfo DefaultIndex="0">
                            <Value>diagonal</Value>
                          </DiscreteInfo>
                        </String>
                        <Int Name="preconditioning-steps" Label="Preconditioning Steps">
                          <BriefDescription>Number of passes to be performed in Jacobi or SSOR preconditioning</BriefDescription>
                          <DefaultValue>2</DefaultValue>
                          <RangeInfo>
                            <Min Inclusive="false">0</Min>
                          </RangeInfo>
                        </Int>
                        <Double Name="relaxation-parameter" Label="Relaxation Parameter">
                          <BriefDescription>Relaxation parameter used in Jacobi and SSOR preconditioning</BriefDescription>
                          <DefaultValue>1.0</DefaultValue>
                          <RangeInfo>
                            <Min Inclusive="false">0.0</Min>
                            <Max Inclusive="false">2.0</Max>
                          </RangeInfo>
                        </Double>
                        <String Name="stopping-criterion" Label="Stopping Criterion">
                          <BriefDescription>Test used to estimate error and determine when convergence
has been reached</BriefDescription>
                          <DiscreteInfo DefaultIndex="1">
                            <Value>||r||</Value>
                            <Value>||r||/||b||</Value>
                          </DiscreteInfo>
                        </String>
                        <Double Name="convergence-criterion" Label="Convergence Criterion">
                          <BriefDescription>Value of error estimate used to determine when convergence
has been reached</BriefDescription>
                          <DefaultValue>1.0e-8</DefaultValue>
                          <RangeInfo>
                            <Min Inclusive="false">0.0</Min>
                            <Max Inclusive="false">0.1</Max>
                          </RangeInfo>
                        </Double>
                        <Int Name="krylov-vectors" Label="Krylov Vectors" AdvanceLevel="1">
                          <BriefDescription>Number of vectors used by GMRES and FGMRES in orthogonalization</BriefDescription>
                          <DefaultValue>50</DefaultValue>
                          <RangeInfo>
                            <Main Inclusive="false">0</Main>
                          </RangeInfo>
                        </Int>
                        <String Name="output-mode" Label="Output Mode" AdvanceLevel="1">
                          <BriefDescription>Controls verbosity of linear solver</BriefDescription>
                          <DiscreteInfo DefaultIndex="0">
                            <Value>none</Value>
                            <Value>errors</Value>
                            <Value>errors+warnings</Value>
                            <Value>summary</Value>
                            <Value>iterates</Value>
                            <Value>full</Value>
                          </DiscreteInfo>
                        </String>
                      </ItemDefinitions>
                    </Group>
                  </ItemDefinitions>
                </Group>
              </ItemDefinitions>
            </Group>
            <!-- Thermal plus flow case-->
            <Group Name="thermal-plus-fluid" Label="Thermal plus Fluid">
              <ItemDefinitions>
                <Group Name="thermal-solver" Label="Thermal Solver">
                  <ItemDefinitions>
                    <Double Name="residual-atol" Label="Absolute residual tolerance">
                      <BriefDescription>The absolute residual tolerance used by the iterative
nonlinear solver of the non-adaptive integrator</BriefDescription>
                      <Categories>
                        <Cat>Heat Transfer</Cat>
                      </Categories>
                      <DefaultValue>0.0</DefaultValue>
                      <RangeInfo>
                        <Min Inclusive="true">0.0</Min>
                      </RangeInfo>
                    </Double>
                    <Double Name="residual-rtol" Label="Relative residual tolerance">
                      <BriefDescription>The relative residual tolerance used by the iterative
nonlinear solver of the non-adaptive integrator</BriefDescription>
                      <Categories>
                        <Cat>Heat Transfer</Cat>
                      </Categories>
                      <DefaultValue>1.0e-8</DefaultValue>
                      <RangeInfo>
                        <Min Inclusive="true">0.0</Min>
                        <Max Inclusive="true">1.0</Max>
                      </RangeInfo>
                    </Double>
                    <Int Name="max-nlk-itr" Label="Max NLK iterations">
                      <BriefDescription>The maximum number of NLK nonlinear solver iterations allowed</BriefDescription>
                      <Categories>
                        <Cat>Heat Transfer</Cat>
                      </Categories>
                      <DefaultValue>5</DefaultValue>
                      <RangeInfo>
                        <Min Inclusive="true">2</Min>
                      </RangeInfo>
                    </Int>
                    <Int Name="max-nlk-vec" Label="Max NLK vectors">
                      <BriefDescription>The maximum number of acceleration vectors to use
in the NLK nonlinear solver</BriefDescription>
                      <Categories>
                        <Cat>Heat Transfer</Cat>
                      </Categories>
                      <DefaultValue>4</DefaultValue>
                      <RangeInfo>
                        <Min Inclusive="true">1</Min>
                      </RangeInfo>
                    </Int>
                    <Void Name="verbose-stepping" Label="Verbose stepping" Optional="true" IsEnabledByDefault="false">
                      <BriefDescription>Enables the output of detailed BDF2 time stepping information</BriefDescription>
                    </Void>
                    <Double Name="nlk-vec-tol" Label="NLK vector drop tolerance" AdvanceLevel="1">
                      <Categories>
                        <Cat>Heat Transfer</Cat>
                        <Cat>Fluid Flow</Cat>
                      </Categories>
                      <DefaultValue>0.001</DefaultValue>
                      <RangeInfo>
                        <Min Inclusive="false">0.0</Min>
                      </RangeInfo>
                    </Double>
                    <Int Name="pc-amg-cycles" Label="Preconditioning V-cycles" AdvanceLevel="1">
                      <BriefDescription>The number of V-cycles to take per preconditioning
step of the nonlinear iteration</BriefDescription>
                      <Categories>
                        <Cat>Heat Transfer</Cat>
                      </Categories>
                      <DefaultValue>1</DefaultValue>
                      <RangeInfo>
                        <Min Inclusive="true">1</Min>
                      </RangeInfo>
                    </Int>
                    <Double Name="cond-vfrac-threshold" Label="Conduction volume fraction" AdvanceLevel="1">
                      <BriefDescription>Cond_Vfrac_Threshold: Material volume fraction threshold
for inclusion in heat conduction when using the non-adaptive integrator.</BriefDescription>
                      <Categories>
                        <Cat>Heat Transfer</Cat>
                      </Categories>
                      <DefaultValue>0.001</DefaultValue>
                      <RangeInfo>
                        <Min Inclusive="true">0.0</Min>
                        <Max Inclusive="true">1.0</Max>
                      </RangeInfo>
                    </Double>
                  </ItemDefinitions>
                </Group>
              </ItemDefinitions>
            </Group>
          </ChildrenDefinitions>
          <DiscreteInfo DefaultIndex="0">
            <Structure>
              <Value Enum="Fluid Only">fluid</Value>
              <Items>
                <Item>fluid</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="Thermal Only">thermal</Value>
              <Items>
                <Item>thermal</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="Fluid plus Thermal">thermal-plus-fluid</Value>
              <Items>
                <Item>thermal-plus-fluid</Item>
                <Item>fluid</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeSystem>
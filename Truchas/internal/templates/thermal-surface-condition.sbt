<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeSystem Version="2">
  <Categories>
    <Cat>Heat Transfer</Cat>
    <Cat>Fluid Flow</Cat>
  </Categories>
  <!-- Attribute Definitions for thermal surface conditions-->
  <Definitions>
    <AttDef Type="thermal-surface-condition" Label="Thermal Surface Condition" BaseType="" Version="0" Unique="true">
      <AssociationsDef Name="ModelAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>face</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <String Name="type" Label="Type">
          <ChildrenDefinitions>
            <!-- DS Boundary Condition-->
            <String Name="ds-boundary-condition" Label="DS Boundary Condition">
              <ChildrenDefinitions>
                <Double Name="temperature" Label="Temperature">
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                  <DefaultValue>0.0</DefaultValue>
                </Double>
                <Double Name="heat-flux" Label="Heat Flux">
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                  <DefaultValue>0.0</DefaultValue>
                </Double>
                <Group Name="htc" Label="HTC" Optional="true" IsEnabledByDefault="false">
                  <ItemDefinitions>
                    <Double Name="heat-transfer-coefficient" Label="Heat Transfer Coefficient (h)">
                      <Categories>
                        <Cat>Heat Transfer</Cat>
                      </Categories>
                      <DefaultValue>0.0</DefaultValue>
                    </Double>
                    <Double Name="reference-temperature" Label="Reference Temperature (T0)">
                      <Categories>
                        <Cat>Heat Transfer</Cat>
                      </Categories>
                      <DefaultValue>0.0</DefaultValue>
                    </Double>
                  </ItemDefinitions>
                </Group>
                <Group Name="radiation" Label="External" Optional="true" IsEnabledByDefault="false">
                  <ItemDefinitions>
                    <Double Name="emissivity" Label="Emissivity (epsilon)">
                      <Categories>
                        <Cat>Heat Transfer</Cat>
                      </Categories>
                      <DefaultValue>0.0</DefaultValue>
                      <RangeInfo>
                        <Min Inclusive="true">0.0</Min>
                        <Max Inclusive="true">1.0</Max>
                      </RangeInfo>
                    </Double>
                    <Double Name="ambient-temperature" Label="Ambient Temperature (T infinity)">
                      <Categories>
                        <Cat>Heat Transfer</Cat>
                      </Categories>
                      <DefaultValue>0.0</DefaultValue>
                    </Double>
                  </ItemDefinitions>
                </Group>
              </ChildrenDefinitions>
              <!-- DS Boundary Condition types-->
              <DiscreteInfo DefaultIndex="0">
                <Structure>
                  <Value Enum="Dirichlet">dirichlet</Value>
                  <Items>
                    <Item>temperature</Item>
                  </Items>
                </Structure>
                <Structure>
                  <Value Enum="Flux">flux</Value>
                  <Items>
                    <Item>heat-flux</Item>
                  </Items>
                </Structure>
                <Structure>
                  <Value Enum="HTC and/or External Radiation">htc-and-radiation</Value>
                  <Items>
                    <Item>htc</Item>
                    <Item>radiation</Item>
                  </Items>
                </Structure>
              </DiscreteInfo>
            </String>
            <!-- DS Interface Condition-->
            <String Name="ds-interface-condition" Label="DS Interface Condition">
              <ChildrenDefinitions>
                <Double Name="heat-transfer-coefficient" Label="Heat Transfer Coefficient (alpha)">
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                  <DefaultValue>0.0</DefaultValue>
                </Double>
                <Double Name="emissivity" Label="Emissivity (epsilon)">
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                  <DefaultValue>0.0</DefaultValue>
                  <RangeInfo>
                    <Min Inclusive="true">0.0</Min>
                    <Max Inclusive="true">1.0</Max>
                  </RangeInfo>
                </Double>
              </ChildrenDefinitions>
              <DiscreteInfo DefaultIndex="0">
                <Structure>
                  <Value Enum="Internal HTC">htc</Value>
                  <Items>
                    <Item>heat-transfer-coefficient</Item>
                  </Items>
                </Structure>
                <Structure>
                  <Value Enum="Gap Radiation">radiation</Value>
                  <Items>
                    <Item>emissivity</Item>
                  </Items>
                </Structure>
              </DiscreteInfo>
            </String>
            <!-- Enclosure Surface-->
            <Void Name="moving" Label="Moving?" Version="0" Optional="true" IsEnabledByDefault="true"></Void>
            <Double Name="emissivity" Label="Emissivity" Version="0">
              <Categories>
                <Cat>Heat Transfer</Cat>
              </Categories>
              <DefaultValue>0.0</DefaultValue>
              <RangeInfo>
                <Min Inclusive="true">0.0</Min>
                <Max Inclusive="true">1.0</Max>
              </RangeInfo>
            </Double>
          </ChildrenDefinitions>
          <DiscreteInfo DefaultIndex="0">
            <Structure>
              <Value Enum="DS Boundary Condition">ds-boundary-condition</Value>
              <Items>
                <Item>ds-boundary-condition</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="DS Interface Condition">ds-interface-condition</Value>
              <Items>
                <Item>ds-interface-condition</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="Enclosure Surface">enclosure-surface</Value>
              <Items>
                <Item>moving</Item>
                <Item>emissivity</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeSystem>

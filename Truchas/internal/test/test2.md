Steps for (re)constructing test case #2
Focused on enclosure radiation

Model: sx_bar_hex.smtk

Surfaces: Thermal Surface Conditions
* Name "Moving Surfaces"
* Type Enclosure Surface
* Moving? OFF
* Emissitivity 0.789
* Associated to set ids: 11, 15, 23, 3

Surfaces: Thermal Surface Conditions
* Name "Stationary Surfaces"
* Type Enclosure Surface
* Moving? OFF
* Emissitivity 0.876
* Associated to set ids: 1, 10, 17, 19, 7, 8, 9

Solver
* Show Level Advanced
* Analysis Type "Thermal Only"
* Absolute enthalpy tolerance 0.0
* Relative enthalpy tolerance 0.001
* Absolute temperature tolerance 0.0
* Relative temperature tolerance 0.001
* Verbose stepping ON
* Max NLK Iterations 6
* Max NLK vectors 2
* NLK convergence tolerance 0.05


Enclosure
* Symmetries - add 3 sub groups
** MirrorX
** MirrorZ
** Periodic RotateZ/4 Folds

* Ignore Blocks ids: 2

* Displacement Sequence
**   0,     0, 0,  0
** 236.22,  0, 0, -0.01
** 472.441, 0, 0, -0.02

* Enclosure File Prefix genre/rad_

* Ambient Temperature 20.0

Other
* Absolute Zero -273.15
